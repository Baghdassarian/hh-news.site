<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%news_rubric}}`.
 */
class m200724_141114_create_news_rubric_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%news_rubric}}', [
            'id' => $this->primaryKey(),
            'news_id' => $this->integer(),
            'rubric_id' => $this->integer(),
        ]);
        $this->createIndex(
            'news_id_rubric_category_id',
            'news_rubric',
            ["rubric_id" , "news_id"],
            1
        );
    }

    public function safeDown()
    {
        $this->dropTable('{{%news_rubric}}');
    }
}
