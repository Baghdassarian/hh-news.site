<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%rubric}}`.
 */
class m200724_124248_create_rubric_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%rubric}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'parent_rubric_id' => $this->integer(),
        ]);
        Yii::$app->db
            ->createCommand()
            ->batchInsert('rubric',
                [
                    'title', 'parent_rubric_id'
                ],
                [
                    ['Общество', '0'],
                    ['День города', '0'],
                    ['городская жизнь', '1'],
                    ['выборы', '1'],
                    ['салюты', '2'],
                    ['детская площадка', '2'],
                    ['0-3 года', '6'],
                    ['3-7 года', '6'],
                    ['Спорт', '0'],
                ])->execute();
    }
//• Общество
//    ◦ городская жизнь
//    ◦ выборы
//• День города
//    ◦ салюты
//    ◦ детская площадка
//        ▪ 0-3 года
//        ▪ 3-7 года
//• Спорт

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%rubric}}');
    }
}
