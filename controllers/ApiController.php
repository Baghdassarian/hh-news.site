<?php


namespace app\controllers;

use yii\helpers\Json;
use yii\web\Response;
use yii\web\Controller;
use app\models\NewsRubric;
use app\models\Rubric;
use Yii;

class ApiController extends Controller
{
    public $enableCsrfValidation = false;

    private $authKey = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0aXRsZSI6Im5ld3NfdGVzdCIsIm5hbWUiOiJIZXJtaW5lIEJhZ2hkYXNzYXJpYW4iLCJpYXQiOjI1MTYyMzkwMjJ9.IpWoUA_-0dfijEqUKMlu46YIz_YnmXusBL5svkAPtfY";

    public function actionDocumentation()
    {

        return $this->render('index', [
            'AuthKey' => $this->authKey,
        ]);
    }

//    public function behaviors()
//    {
//        $behaviors = parent::behaviors();
//
//        $behaviors['authenticator'] = [
//            'class' => CompositeAuth::className(),
//            'authMethods' => [
//                HttpBasicAuth::className(),
//                HttpBearerAuth::className(),
//                QueryParamAuth::className(),
//            ],
//        ];
//        return $behaviors;
//
//    }


    public function auth()
    {

        $request = Yii::$app->request;
        $authHeader = $request->getHeaders()->get('authkey');
        $res = [];
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (empty($authHeader)) {
            $res['error'] = "oops, invalid authKey";
        } else  if ( $authHeader !== $this->authKey ) {
            $res['error'] = "authKey is not match";
        }

        if(isset($res['error'] )) {
            die(Json::encode($res));
        }
    }


    public function actionGetNewsByRubric()
    {
        $this->auth();
        $request = Yii::$app->request->post();
        $response = Yii::$app->response;
        $res = [];
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (isset($request["rubric"])) {
            $rubric = Rubric::findOne([
                'title' => $request["rubric"]
            ]);
            if (!empty($rubric)) {

                $query = NewsRubric::find()
                    ->select(['news.title', 'news.description', 'rubric_id'])
                    ->leftJoin('news', 'news_rubric.news_id=news.id')
                    ->leftJoin('rubric', 'news_rubric.rubric_id=rubric.id')
                    ->where(['news_rubric.rubric_id' => $rubric->id])
                    ->orWhere(['rubric.parent_rubric_id' => $rubric->id])
                    ->asArray()->all();

                $res = $query;
            } else {
                $response->statusCode = 403;
                $res['error'] = ['Rubric not found.'];
            }
        } else {
            $response->statusCode = 403;
            $res['error'] = ['Rubric is required.'];
        }
        $response->format = \yii\web\Response::FORMAT_JSON;
        return Json::encode($res);
    }

    function buildTree(array $elements, $parentId = 0)
    {
        $branch = array();
        foreach ($elements as $element) {
            if (intval($element['parent_rubric_id']) == $parentId) {
                $children = $this->buildTree($elements, $element['id']);
                if ($children) {
                    $element['children'] = $children;
                }
                $branch[$element['title']] = [
                    "id" => $element["id"],
                    "sub" => $children
                ];
            }
        }
        return $branch;
    }

    public function actionGetRubricLevels()
    {
        $this->auth();
        $response = Yii::$app->response;
        Yii::$app->response->format = Response::FORMAT_JSON;
        $rubrics = Rubric::find()->asArray()->all();
        $response->format = \yii\web\Response::FORMAT_JSON;
        return Json::encode($this->buildTree($rubrics));
    }
}