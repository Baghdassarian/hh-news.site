<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';

use yii\helpers\Html; ?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Добро пожаловать!</h1>
        <p class="lead">Добро пожаловать в тестовое приложение</p>
    </div>

    <div class="body-content">
        <div class="row">
            <p>
                Наш каталог будет содержать 2 вида объектов:
            </p>
            <ul>
                <li><?= Html::a('Новость', ['/news']);?></li>
                <li><?= Html::a('Рубрика', ['/rubric']);?></li>
            </ul>
        </div>
    </div>
</div>
