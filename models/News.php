<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "news".
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $description
 */
class News extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'description' => Yii::t('app', 'Description'),
        ];
    }
    public function getRsubrics()
    {
        return $this->hasMany(Rubric::tableName(), ['id' => 'news_id'])
            ->via(NewsRubric::tableName(), ['rubric_id' => 'id']);
    }
    public function getRubrics()
    {
        return $this->hasMany(NewsRubric::tableName(), ['id' => 'news_id']);

    }
    public function getNewsRubrics()
    {
        return $this->hasMany(NewsRubric::tableName(), ['id' => 'news_id']);

    }
}
