<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "rubric".
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $parent_rubric_id
 */
class Rubric extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rubric';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'string', 'max' => 255],
            [['parent_rubric_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'parent_rubric_id' => Yii::t('app', 'Parent'),
        ];
    }

}
